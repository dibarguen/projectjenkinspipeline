package com.grupoaval.portalafc.pruebas.stepdefinitions;

import java.util.List;

import com.grupoaval.portalafc.pruebas.models.Usuario;
import com.grupoaval.portalafc.pruebas.steps.InicioDeSesionSteps;

import cucumber.api.java.es.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.thucydides.core.annotations.Steps;

public class InicioDeSesionStepDefinitions {

	@Steps
	InicioDeSesionSteps inicioDeSesionSteps;
	
	@Steps
	InicioDeSesionSteps anadirProductoAlCarro;
	
	String usuario;
	
	@Dado("^que David ingresa al portal AFC$")
	public void queDavidIngresaAlPortalAFC() {
		inicioDeSesionSteps.abrirPortalAFC();
	}

	@Cuando("^ingresa su usuario y clave$")
	public void ingresaSuUsuarioYClave(List<Usuario> listaDeUsuarios) {
		usuario=listaDeUsuarios.get(0).getUsuario();
		inicioDeSesionSteps.escribirUsuario(usuario);
		inicioDeSesionSteps.escribirClave(listaDeUsuarios.get(0).getClave());
	}

	@Cuando("^presiona la tecla Enter$")
	public void presionaLaTeclaEnter() {
		inicioDeSesionSteps.presionarEnterEnCampoClave();
	}

	@Entonces("^deberia ver el siguiente mensaje en pantalla:$")
	public void deberiaVerElSiguienteMensajeEnPantalla(List<String> listaDeMensajes) {
		inicioDeSesionSteps.validarPresenciaYContenidoDelMensaje(listaDeMensajes.get(0));
	}

	@Entonces("^su nombre y usuario registrados deberian aparecer en el menu de Cuenta:$")
	public void suNombreYUsuarioRegistradosDeberianAparecerEnElMenuDeCuenta(List<String> listaNombre) {
		inicioDeSesionSteps.clicEnBotonDeCuenta();
		inicioDeSesionSteps.validarPresenciaYContenidoDeEtiquetaNombreUsuario(listaNombre.get(0));
		inicioDeSesionSteps.validarPresenciaYContenidoDeEtiquetaUsuario(usuario);
	}

	@Cuando("^hace clic en el boton de Log In$")
	public void haceClicEnElBotonDeLogIn() {
		inicioDeSesionSteps.clicEnIngresar();
	}

	@Entonces("^no deberia ver el siguiente mensaje en pantalla:$")
	public void noDeberiaVerElSiguienteMensajeEnPantalla(List<String> listaDeMensajes) {
		inicioDeSesionSteps.validarQueNoAparezcaElMensajeEnPantalla(listaDeMensajes.get(0));
	}

	@Entonces("^no deberia ver su nombre en el menu de Cuenta$")
	public void noDeberiaVerSuNombreEnElMenuDeCuenta(List<String> listaDeNombres) {
		inicioDeSesionSteps.clicEnBotonDeCuenta();
		inicioDeSesionSteps.validarQueNoAparezcaUnNombreDeUsuario(listaDeNombres.get(0));
		inicioDeSesionSteps.validarQueNoAparezcaUnUsuario(usuario);
	}

	@Cuando("^hace clic sobre el campo Usuario$")
	public void haceClicSobreElCampoUsuario() {
		inicioDeSesionSteps.clicEnCampoUsuario();
	}

	@Cuando("^hace clic sobre el campo Clave$")
	public void haceClicSobreElCampoClave() {
		inicioDeSesionSteps.clicEnCampoClave();
	}
	
	@Cuando("^selecciona una categoria$")
	public void clicSobreCategoria() {
		anadirProductoAlCarro.categoria();
		
	}

	
	
	
}

package com.grupoaval.portalafc.pruebas.runners;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features = { "src\\test\\resources\\com\\grupoaval\\portalafc\\pruebas\\features\\InicioDeSesion.feature\\AñadirProductoalCarro.feature" }, glue = {
		"com.grupoaval.portalafc.pruebas.stepdefinitions" }, tags = "@SmokeTest", snippets = SnippetType.CAMELCASE)
public class InicioDeSesionRunner {

}

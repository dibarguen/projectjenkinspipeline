#language:es

Característica: Agregar al carrito de compras
  I want to use this template for my feature file

  Antecedentes:
    Dado que David ingresa al portal AFC
    Cuando ingresa su usuario y clave
      | Usuario | Clave   |
      | dagan01 | dag1996 |
    Y presiona la tecla Enter
    

  @Smoke
  Escenario: Title of your scenario
    Cuando selecciona una categoria
      | Celulares |
    Y selecciona el producto "Celular XIAOMI Mi A2 Lite DS 4G Dorado"
    Y se agrega al carrito
    Entonces valida que aparezca el siguiente mensaje:
      | El producto Celular XIAOMI Mi A2 Lite DS 4G Dorado ha sido añadido al carrito. |
    Y valida que aparezca el producto agregado en el acceso directo del carrito
    Cuando David ingresa al menu de Cuenta
    Y selecciona el carrito de compras
    Entonces valida que aparezca el producto agregado


#language: es
Característica: Inicio de sesion
  Como usuario registrado, quiero iniciar sesion en el portal AFC
  para poder hacer compras.

  @SmokeTest
  Escenario: Inicio de sesion exitoso presionando Enter
    Dado que David ingresa al portal AFC
    Cuando ingresa su usuario y clave
      | Usuario | Clave   |
      | dagan01 | dag1996 |
    Y presiona la tecla Enter
    Entonces deberia ver el siguiente mensaje en pantalla:
      | Login Exitoso |
    Y su nombre y usuario registrados deberian aparecer en el menu de Cuenta:
      | David Ayala |

  @SmokeTest
  Escenario: Inicio de sesion exitoso haciendo clic en el boton de Log In
    Dado que David ingresa al portal AFC
    Cuando ingresa su usuario y clave
      | Usuario | Clave   |
      | dagan01 | dag1996 |
    Y hace clic en el boton de Log In
    Entonces deberia ver el siguiente mensaje en pantalla:
      | Login Exitoso |
    Y su nombre y usuario registrados deberian aparecer en el menu de Cuenta:
      | David Ayala |

  @SmokeTest
  Esquema del escenario: Inicio de sesión no exitoso
    Dado que David ingresa al portal AFC
    Cuando ingresa su usuario y clave
      | Usuario   | Clave   |
      | <Usuario> | <Clave> |
    Y hace clic en el boton de Log In
    Entonces no deberia ver el siguiente mensaje en pantalla:
      | Login Exitoso |
    Y no deberia ver su nombre en el menu de Cuenta
      | <Nombre> |

    Ejemplos: 
      | Usuario    | Clave    | Nombre      |
      | dagan01    | abcd1234 | David Ayala |
      | dagan10000 | dag1996  | David Ayala |
      | dagan10000 | abcd1234 | David Ayala |

  @SmokeTest
  Escenario: Mensaje de Usuario Obligatorio
    Dado que David ingresa al portal AFC
    Cuando hace clic sobre el campo Usuario
    Y hace clic sobre el campo Clave
    Entonces deberia ver el siguiente mensaje en pantalla:
      | El Usuario es requerido |

  @SmokeTest
  Escenario: Mensaje de Clave Obligatoria
    Dado que David ingresa al portal AFC
    Cuando hace clic sobre el campo Clave
    Y hace clic sobre el campo Usuario
    Entonces deberia ver el siguiente mensaje en pantalla:
      | Contraseña es requerida |

  @SmokeTest
  Escenario: Mensaje de Clave Obligatoria
    Dado que David ingresa al portal AFC
    Cuando hace clic sobre el campo Clave
    Y hace clic sobre el campo Usuario
    Entonces deberia ver el siguiente mensaje en pantalla:
      | Contraseña es requerida |
		
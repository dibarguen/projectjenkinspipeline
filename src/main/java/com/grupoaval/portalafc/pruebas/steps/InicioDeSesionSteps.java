package com.grupoaval.portalafc.pruebas.steps;


import com.grupoaval.portalafc.pruebas.pageobjects.PortalAFCHome;

import net.thucydides.core.annotations.Step;

public class InicioDeSesionSteps {

	PortalAFCHome portalAFCHome= new PortalAFCHome();
	
	@Step
	public void abrirPortalAFC() {
		portalAFCHome.open();
	}
	
	@Step
	public void escribirUsuario(String usuario) {
		portalAFCHome.escribirUsuario(usuario);
	}
	
	@Step
	public void escribirClave(String clave) {
		portalAFCHome.escribirClave(clave);
	}
	
	@Step
	public void clicEnIngresar() {
		portalAFCHome.clicEnIngresar();
	}
	
	@Step
	public void presionarEnterEnCampoClave() {
		portalAFCHome.presionarEnterEnCampoClave();
	}

	@Step
	public void validarPresenciaYContenidoDelMensaje(String mensaje) {
		portalAFCHome.validarPresenciaYContenidoDeMensaje(mensaje);
	}
	
	@Step
	public void clicEnBotonDeCuenta() {
		portalAFCHome.clicEnBotonDeCuenta();
	}
	
	@Step
	public void validarPresenciaYContenidoDeEtiquetaUsuario(String usuario) {
		portalAFCHome.validarPresenciaYContenidoDeEtiquetaUsuario(usuario);
	}
	
	@Step
	public void validarPresenciaYContenidoDeEtiquetaNombreUsuario(String nombreDeUsuario) {
		portalAFCHome.validarPresenciaYContenidoDeEtiquetaNombreUsuario(nombreDeUsuario);
	}
	
	@Step
	public void validarQueNoAparezcaElMensajeEnPantalla(String mensaje) {
		portalAFCHome.validarQueNoAparezcaElMensajeEnPantalla(mensaje);
	}
	
	@Step
	public void validarQueNoAparezcaUnNombreDeUsuario(String nombreDeUsuario) {
		portalAFCHome.validarQueNoAparezcaUnNombreDeUsuario(nombreDeUsuario);
	}
	
	@Step
	public void validarQueNoAparezcaUnUsuario(String usuario) {
		portalAFCHome.validarQueNoAparezcaUnUsuario(usuario);
	}
	
	@Step
	public void clicEnCampoUsuario() {
		portalAFCHome.clicEnCampoUsuario();
	}
	
	@Step
	public void clicEnCampoClave() {
		portalAFCHome.clicEnCampoClave();
	}
	
	@Step
	public void categoria() {
		portalAFCHome.categoria(); System.out.println("CATEGORIA ESCOGIDA");
	}
	
	
	
}

package com.grupoaval.portalafc.pruebas.pageobjects;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import net.serenitybdd.core.pages.PageObject;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("http://aae49f8cf479211eaad3c0ed862d58a0-8482866.us-east-1.elb.amazonaws.com/")
public class PortalAFCHome extends PageObject {

	By txtUsuario = By.xpath("//input[@formcontrolname='user']");

	By txtClave = By.xpath("//input[@formcontrolname='password']");

	By btnIngresar = By.xpath("//button[@class='mat-elevation-z6 mat-fab mat-primary']");

	By btnCategoria = By.cssSelector(".col:nth-child(4) img");
	/*
	 * @FindBy(xpath =
	 * "//simple-snack-bar[@class='mat-simple-snackbar ng-star-inserted']//span")
	 * WebElement lblMensajeLogIn;
	 */

	By btnCuenta = By.xpath("//div[@class='top-menu']/a");

	 By lblNombreDeUsuario=By.xpath("//div[@class='user-info ng-star-inserted']/p");

	 By lblUsuario=By.tagName("small");
	 
	 


	public void escribirUsuario(String usuario) {
		find(txtUsuario).sendKeys(usuario);

	}

	public void escribirClave(String clave) {
		find(txtClave).sendKeys(clave);
	}

	public void clicEnIngresar() {
		find(btnIngresar).click();
	}

	public void presionarEnterEnCampoClave() {
		find(txtClave).sendKeys(Keys.ENTER);
	}

	public void validarPresenciaYContenidoDeMensaje(String mensaje) {
		/*
		 * if (lblMensajeLogIn.isDisplayed()) { assertThat(lblMensajeLogIn.getText(),
		 * equalTo(mensaje));
		 * 
		 * } else { throw new AssertionError("El elemento " + lblMensajeLogIn.toString()
		 * + " no está presente en la pantalla", null); }
		 */
		waitForAngularRequestsToFinish();
		find(btnCategoria).click();
		System.out.println("Aqui toy");
		WebElement lblMensaje = getDriver().findElement(By.xpath("//*[text()='" + mensaje + "']"));
		if (lblMensaje.isDisplayed()) {
			assertThat(lblMensaje.getText(), equalTo(mensaje));

		} else {
			throw new AssertionError("El elemento " + lblMensaje.toString() + " con mensaje '" + mensaje
					+ "' no está presente en la pantalla", null);
		}
	}

	public void validarQueNoAparezcaElMensajeEnPantalla(String mensaje) {
		/*
		 * if (lblMensajeLogIn.isDisplayed()) { assertThat(lblMensajeLogIn.getText(),
		 * not(equalTo(mensaje))); } else { throw new AssertionError("El elemento " +
		 * lblMensajeLogIn.toString() + " no está presente en la pantalla", null); }
		 */
		waitForAngularRequestsToFinish();
		WebElement lblMensaje = getDriver().findElement(By.xpath("//*[text()='" + mensaje + "']"));
		if (lblMensaje.isDisplayed()) {
			throw new AssertionError("El elemento " + lblMensaje.toString() + " con mensaje '" + mensaje
					+ "' está presente en la pantalla", null);
		}
	}

	public void validarPresenciaYContenidoDeEtiquetaNombreUsuario(String nombreDeUsuario) {
		waitForAngularRequestsToFinish();
		if (find(lblNombreDeUsuario).isVisible()) {
			assertTrue("El elemento " + lblNombreDeUsuario.toString() + " no contiene el nombre de usuario.",find(lblNombreDeUsuario).getText().contains(nombreDeUsuario));
			assertTrue("El elemento " + lblNombreDeUsuario.toString() + " no está visible.",
					find(lblNombreDeUsuario).isVisible());

		} else {
			throw new AssertionError("El elemento " + lblNombreDeUsuario.toString()
					+ " no está visible en la pantalla", null);
		}
	}

	public void validarPresenciaYContenidoDeEtiquetaUsuario(String usuario) {
		waitForAngularRequestsToFinish();
		if (find(lblUsuario).isVisible()) {
			assertTrue("El elemento " + lblUsuario.toString() + " no contiene el usuario.",find(lblUsuario).getText().contains(usuario));
			assertTrue("El elemento " + lblUsuario.toString() + " no está visible.",
					find(lblUsuario).isVisible());

		} else {
			throw new AssertionError("El elemento " + lblUsuario.toString()
					+ " no está visible en la pantalla", null);
		}
	}

	public void clicEnBotonDeCuenta() {
		find(btnCuenta).click();
	}

	public void validarQueNoAparezcaUnNombreDeUsuario(String nombreDeUsuario) {
		if (getDriver().findElements(lblNombreDeUsuario).size() > 0) {
			throw new AssertionError("El elemento " + lblNombreDeUsuario.toString()
					+ " no debería estar presente en la pantalla", null);
		}
	}

	public void validarQueNoAparezcaUnUsuario(String usuario) {
		if (getDriver().findElements(lblUsuario).size() > 0) {
			throw new AssertionError(
					"El elemento " + lblUsuario.toString() + " no debería estar presente en la pantalla",
					null);
		}
	}

	public void clicEnCampoUsuario() {
		find(txtUsuario).click();
	}

	public void clicEnCampoClave() {
		find(txtClave).click();
	}
	
	public void clicCuenta() {
		find(btnIngresar).click();
	}
	
	public void categoria() {
		find(btnCategoria).click();
	}
	


}
